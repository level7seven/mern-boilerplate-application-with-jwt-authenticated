import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { get_all_users } from "../../../redux/actions/users";
import { loadUser } from "../../../redux/actions/auth";
import Spinner from "../../elements/spinner/Spinner";
import avatar from "../../../assets/img/avatar.webp";
import { RiInformationLine } from "react-icons/ri";

const Users = ({ get_all_users, users, auth, loadUser }) => {
  const [spinner, set_spinner] = useState(true);

  useEffect(() => {
    loadUser().then(get_all_users().then(set_spinner(false)));
  }, []);

  console.log(auth);
  return (
    <div className='users'>
      <div className='container'>
        <div className='row'>
          {!spinner ? (
            <>
              {users && users.length > 0 ? (
                <>
                  {users.map((item) => (
                    <div className='col col-sm-12 col-md-6 users'>
                      <div className='card mb-3 p-2'>
                        <div className='row'>
                          <div className='col col-sm-3 avatar'>
                            <img src={avatar} alt='avatar' />
                          </div>
                          <div className='col col-sm-9 user-info'>
                            <h5>@{item.username}</h5>
                            <p>{item.name}</p>
                            <p>{item.email}</p>
                          </div>
                          {auth &&
                          auth.user &&
                          auth.user.user._id === item._id ? (
                            <div className='user-more-info' title='more info'>
                              <RiInformationLine />
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  ))}
                </>
              ) : (
                "no user"
              )}
            </>
          ) : (
            <Spinner />
          )}
        </div>
      </div>
    </div>
  );
};

Users.propTypes = {
  get_all_users: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users.users,
  auth: state.auth,
});

export default connect(mapStateToProps, { get_all_users, loadUser })(Users);
