import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../../redux/actions/auth";
import Login from "./Login";

const LoginForm = ({ login, isAuthenticated, auth }) => {
  const [login_info, set_login_info] = useState({
    username: "",
    password: "",
  });

  const onChange = (e) => {
    set_login_info({
      ...login_info,
      [e.target.id]: e.target.value,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();

    login(login_info);
  };

  console.log(auth);
  return (
    <form onSubmit={onSubmit}>
      <div className='form-group'>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='username'>
            Username
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='text'
              className='form-control'
              id='username'
              value={login_info.username}
              onChange={onChange}
              placeholder='username'
            />
          </div>
        </div>
        <div className='mb-2'>
          <label className='sr-only' htmlFor='password'>
            Password
          </label>
          <div className='input-group'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>@</div>
            </div>
            <input
              type='password'
              className='form-control'
              id='password'
              value={login_info.password}
              onChange={onChange}
              placeholder='password'
            />
          </div>
        </div>
        <div className='mb-2'>
          <button type='submit' className='btn btn-dark btn-block'>
            Login
          </button>
        </div>
      </div>
    </form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(LoginForm);
