import { GET_ALL_USERS } from "../actions/type";

const initialState = {
  users: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_USERS:
      return {
        ...state,
        users: payload.data,
      };

    default:
      return state;
  }
}
