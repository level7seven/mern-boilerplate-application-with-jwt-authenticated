import axios from "axios";
import { GET_ALL_USERS } from "./type";
import { api } from "../api/api";
import { NotificationManager } from "react-notifications";

// get all users
export const get_all_users = () => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      "x-auth-token": localStorage.getItem("token"),
    },
  };

  try {
    const res = await axios.get(`${api}/users`, config);

    dispatch({
      type: GET_ALL_USERS,
      payload: res.data,
    });
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach((error) => {
        NotificationManager.error(error.msg, "Error Get All Users");
      });
    }
  }
};
